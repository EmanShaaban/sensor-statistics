package models

import scala.collection.mutable.Map

object models {


  case class SensorStats(sensorId: String, min: Double, avg: (Double, Int), max: Double)

  class Stats() {
    var processedFiles = 0
    var processedMeasurements = 0
    var failedMeasurements = 0
    var sensors: Map[String, SensorStats] = Map() //ex: Map(s1 -> SensorStats(s1, min, avg, max)
  }

}
