import utils.Helpers._
import models.models._

object Main extends App {
  if (args.length == 0) {
    println("Please enter directory path.")
  } else {
    val path = args(0)
    println(s"Directory path: ${path}")

    // get csv files from directory
    val files = getListOfFiles(path)


    val stats = new Stats()
    stats.processedFiles = files.length

    for (file <- files) {
      loadFile(file, stats)
    }


    println("===================== Stats ============================")
    println(s"Num of processed files: ${stats.processedFiles}")
    println(s"Num of processed measurements: ${stats.processedMeasurements}")
    println(s"Num of failed measurements:${stats.failedMeasurements}")

    println("\n")
    println("Sensors with highest avg humidity:")
    println("\n")
    println("sensor-id,min,avg,max")

    val sensors = stats.sensors
    for (s <- sensors) {
      val data = s._2
      val avg = data.avg._1 / data.avg._2

      println(s"${data.sensorId}, ${data.min}, ${avg}, ${data.max}")
    }
  }


}
