package utils

import java.io.File

import scala.math._

import models.models._

object Helpers {

  def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList.filter { file =>
        file.getName.endsWith("csv")
      }
    } else {
      println("Invalid or empty Directory!")
      List[File]()
    }
  }


  def loadFile(path: File, stats: Stats) = {
    val bufferedSource = io.Source.fromFile(path)

    for (line <- bufferedSource.getLines.drop(1)) {
      val row = line.split(",").map(_.trim)
      val sensorId = row(0)
      val humidity = row(1).toDouble
      val sensors = stats.sensors


      // Increment processed rows
      stats.processedMeasurements += 1

      if (humidity.isNaN) { // Increment NaN measurements
        stats.failedMeasurements += 1
      }

      val currentSensor = sensors.get(sensorId) // Check if current sensor exists in the sensors map

      currentSensor match {
        case Some(sensorStats) => sensors(sensorId) = calcStats(sensorId, sensorStats, humidity) // compare and update stats
        case None => {
          val avg = if (humidity.isNaN) (humidity, 0) else (humidity, 1)
          sensors(sensorId) = SensorStats(sensorId, humidity, avg, humidity) // add initial stats for sensor
        }
      }

    }

    bufferedSource.close
  }


  def calcStats(sensorId: String, statsValue: SensorStats, humidity: Double) = {
    var minHumidity = min(statsValue.min, humidity)
    var maxHumidity = max(statsValue.max, humidity)

    var avgSum = statsValue.avg._1
    var avgCount = statsValue.avg._2

    if (humidity.isNaN) {
      // keeps the old stats if the new is NaN
      if (!statsValue.min.isNaN) minHumidity = statsValue.min
      if (!statsValue.max.isNaN) maxHumidity = statsValue.max
    } else {
      avgSum = if (avgSum.isNaN) humidity else avgSum + humidity
      avgCount += 1

      // Update the old stats if its NaN
      if (statsValue.min.isNaN) minHumidity = humidity
      if (statsValue.max.isNaN) maxHumidity = humidity
    }


    SensorStats(sensorId, minHumidity, (avgSum, avgCount), maxHumidity)

  }
}

