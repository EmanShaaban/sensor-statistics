package stats



import utils.Helpers._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class AppSpec extends AnyWordSpec with Matchers{


  val files = getListOfFiles("src/test/scala/resources/data")


  "Stats" should {
    "Should read csv files only" in {
      files.length shouldEqual (2)
    }
  }
}
